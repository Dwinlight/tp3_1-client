package modele;


import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.HyperLiens;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Optional;
import static configuration.JAXRS.*;

public interface Bibliotheque {
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
	Optional<HyperLien<Livre>> chercher(Livre l);
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    @Path(SOUSCHEMIN_CATALOGUE)
	HyperLiens<Livre> repertorier();
	
}
