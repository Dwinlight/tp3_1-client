package modele;

import infrastructure.jaxrs.HyperLien;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface Archive {

	Livre sousRessource(IdentifiantLivre id) ;
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	Livre getRepresentation(@PathParam("id") IdentifiantLivre id);
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	HyperLien<Livre> ajouter(Livre l);
}
