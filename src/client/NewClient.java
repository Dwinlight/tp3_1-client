package client;
import static org.junit.Assert.*;
import configuration.JAXRS;
import infrastructure.jaxrs.ClientRessource;
import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.Outils;
import modele.BibliothequeArchive;
import modele.ImplemLivre;
import modele.Livre;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;
import java.lang.reflect.Proxy;
import java.util.Optional;

public class NewClient {
    private static final String ADRESSE = JAXRS.SERVEUR + JAXRS.CHEMIN;
    @Test
    public static void main(String[] args) {

        System.out.println("*************");

        WebTarget cible = JAXRS.client().target(ADRESSE);
        BibliothequeArchive biblio = WebResourceFactory.newResource(BibliothequeArchive.class, cible);

        System.out.println("Biblio (proxy) : " + Proxy.isProxyClass(biblio.getClass()));


        System.out.println("*** 1. Ajouter 10 livres ***");

        Livre l1=null;

        HyperLien<Livre> r1 = null;

        for (int i = 0; i < 10; i++) {
            l1 = new ImplemLivre("Livre N°: "+i);
            r1 = biblio.ajouter(l1);
            assertEquals("http://localhost:8080/TP31_server/bibliotheque/"+i,r1.getUri().toString());
        }

        System.out.println("*** 2. Chercher un livre présent ***");
        Optional<HyperLien<Livre>> rezPresent = biblio.chercher(l1);
        //System.out.println("GET 6 - uri : " + rezPresent);
        assertTrue(rezPresent.isPresent());
        assertEquals("http://localhost:8080/TP31_server/bibliotheque/"+9,rezPresent.get().getUri().toString());

        System.out.println("*** 3. Chercher un livre absent ***");
        Optional<HyperLien<Livre>> rezAbs = biblio.chercher(new ImplemLivre("absent"));
        assertFalse(rezAbs.isPresent());

        System.out.println("*** 4. Récupérer un livre à partir de l'hyperlien ***");

        Livre l4 = ClientRessource.proxy(rezPresent.get(), Livre.class);
        System.out.println(l4.getClass());
        assertEquals("Livre N°: 9", l4.getTitre());

        Livre l3b = ClientRessource.representation(rezPresent.get(), Livre.class, JAXRS.OBJET_TYPE_MEDIA);

        assertEquals(l4.getTitre(),l3b.getTitre());
        System.out.println(l3b.getTitre());








        System.exit(0);
    }
}
