package infrastructure.jaxrs;

import infrastructure.langage.Types;
import org.glassfish.hk2.utilities.reflection.ParameterizedTypeImpl;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;


@Priority(Priorities.HEADER_DECORATOR+2)
public class AdapterClientReponsesPUTEnOption implements ReaderInterceptor {
    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
        
    	if (context.getType() == Optional.class){
    		Type typeGen = context.getGenericType();
    		ParameterizedType pt = (ParameterizedType)(typeGen);
            Type type = pt.getActualTypeArguments()[0];
            pt = new ParameterizedTypeImpl(type);
            context.setType(Types.convertirTypeEnClasse(type));
            return Optional.of(context.proceed());

        }
    	return context.proceed();
    }
}
